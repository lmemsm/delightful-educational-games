# delightful educational games [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of of Free, Libre and Open Source educational games.

Be sure to check out other [Delightful Lists](https://codeberg.org/teaserbot-labs/delightful).

## Contents

- [List](#list)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## List

| Program | License | Summary |
| --- | :---: | --- |
|[2048](https://github.com/gabrielecirulli/2048)|MIT|Online puzzle game.|
|[9P](https://f-droid.org/en/packages/se.tube42.p9.android/)|GPLv2.0|Android word puzzle game.|
|[AKFQuiz](http://akfquiz.akfoerster.de/)|GNU GPLv3.0|Make your own quizzes.|
|[AlexGames](https://f-droid.org/en/packages/net.alexbarry.alexgames/)|AGPLv3.0|Word puzzles and other logic games for Android.|
|[Alice](http://www.alice.org/) and [Storytelling Alice](http://www.alice.org/kelleher/storytelling/index.html)|BSD 2 clause|Alice is an innovative 3D programming environment that makes it easy to create an animation for telling a story, playing an interactive game or a video to share on the web.|
|[Anagramarama](http://code.google.com/p/anagramarama/) and [fork](http://www.patthoyts.tk/gitweb.cgi?p=anagramarama;a=summary)|GNU GPLv2.0|Educational game for finding anagrams.|
|[Anagramica](https://github.com/binarymax/anagramica)|MIT|Online word game for finding anagrams.|
|[Anki](https://apps.ankiweb.net/)|GNU GPLv3.0|Anki is a program which makes remembering things easy.|
|[Asymptopia Crossword Builder](https://sourceforge.net/projects/axwb/)|GNU GPLv2.0|Asymptopia Crossword Builder is a JavaScript education application that runs in any modern internet browser but does not require an internet connection.|
|[Atomix](https://wiki.gnome.org/Apps/Atomix)|GNU GPLv2.0|Atomix is a puzzle game where you move atoms to build a molecule.|
|[Brainjogging](https://f-droid.org/en/packages/de.telefongarten.brainjogging/)|GNU GPLv3.0|App to train you brain with mini games for Android and IOS.|
|[Caph Game](https://sourceforge.net/projects/caphgame/)|GNU GPLv3.0|Sandbox game based on physics.|
|[Childsplay](http://www.schoolsplay.org/)|GNU GPLv2.0|Childsplay is a suite of educational games for young children similar to gcompris.|
|[comistat Games Collection](http://comisat-games.sourceforge.net/en/index.html)|GNU GPLv2.0|Comisat Games Collection is an all-in-one free collection of games.|
|[Connectagram](https://github.com/gottcode/connectagram)|GNU GPLv3.0|Word unscrambling game.|
|[Cutemaze](https://github.com/gottcode/cutemaze)|GNU GPLv3.0|Maze game.|
|[cwordle](https://github.com/velorek1/cwordle)|MIT|Wordle clone for Unix and Windows terminals in C.|
|[Dual N-Back Lite](https://sourceforge.net/projects/dualnbacklite/)|GNU GPLv3.0|Designed to mimic the simple training scheme outlined in Jaeggi's paper on developing working memory.|
|[eduActiv8](https://sourceforge.net/projects/eduactiv8/)|GNU GPLv3.0|eduActiv8 is a free Open Source multi-platform educational application that aims to assist in learning various early education topics - from learning the alphabet and new words, colours, time to a wide range of math-related subjects.|
|[Everything Attacks](http://identicalsoftware.com/everythingattacks/) and [source](https://github.com/dulsi/everythingattacks)|GNU GPLv3 and CC|Online geography game.|
|[fltkmm](https://www.muquit.com/muquit/software/fltkmm/fltkmm.html)|GNU GPLV3.0|FLTK Mastermind is a simple logic game.|
|[Follow Me](https://github.com/gamedolphin/follow_me_javascript_simon_clone)|MIT|Online memory game.|
|[Forkyz](https://gitlab.com/Hague/forkyz)|GNU GPLv3.0|Displays and lets you play crossword puzzles in a variety of formats on Android.|
|[Grabagram](https://f-droid.org/packages/uk.co.busydoingnothing.anagrams/)|GNU GPLv3.0|Android anagram game to play online with friends.|
|[Graphwar](http://www.graphwar.com/)|GNU GPLv3.0 or later|Graphwar is an artillery game in which you must hit your enemies using mathematical functions.|
|[Games for French pre-school](https://sourceforge.net/projects/atnag/)|GNU GPLv2.0|Games for French pre-school including memory, letters falling, puzzle, labyrinth, letters & number, domino, counting, etc.|
|[Garith](http://www.thregr.org/~wavexx/software/garith/)|GNU LGPLv2.1|A game to improve your arithmetic abilities.|
|[GCompris](https://gcompris.net/index-en.html)|GNU GPLv3.0|GCompris is an educational software suite comprising of numerous activities for children aged 2 to 10.|
|[Gridle](https://github.com/billthefarmer/gridle)|GNU GPLv3.0|Android word game to move letters in the grid to make a gridle of words.| 
|[Gurgle](https://github.com/billthefarmer/gurgle)|GNU GPLv3.0|Android word game to guess a word.|
|[Hexalate](https://github.com/gottcode/hexalate)|GNU GPlv3.0|Color matching game.|
|[Hextris](https://github.com/Hextris/hextris)|GNU GPLv3.0|Online puzzle game inspired by Tetris.|
|[How Many Blocks?](http://sourceforge.net/projects/howmanyblocks/)|GNU LGPLv2.1|Challenge the limit of your 3D imagination.|
|[Jigsaw](https://f-droid.org/en/packages/io.gitlab.derjosef.jigsaw/)|GNU GPLv3.0|Android Jigsaw puzzle using pictures on your phone.|
|[jMemorize](http://sourceforge.net/projects/jmemorize/)|GNU GPLv2.0|jMemorize is a free Open Source Java application that manages your learning processes by using flashcards and the famous Leitner system.|
|[Johnny](http://sourceforge.net/projects/johnnysimulator/)|GNU GPLv3.0|A Simulator of a Simple von-Neumann Computer.|
|[Kanatest](http://clayo.org/kanatest/)|GNU GPLv3.0|Kanatest is a Japanese kana (Hiragana and Katakana) simple flashcard tool.|
|[Kumquats](https://github.com/dozingcat/Kumquats)|GNU GPLv3.0|Android and desktop word game where you place letters to create intersecting words as quickly as possible.|
|[Learning Basic on Mars](https://github.com/dulsi/LearningBasic/)|GNU GPLv3.0|Simple missions to teach BASIC in a fun setting.|
|[Lexica](https://f-droid.org/packages/com.serwylo.lexica/)|GNU GPLv3.0|App (for Android) to find words in a grid within a time limit.|
|[libpuz](https://code.google.com/archive/p/puz/)|GNU GPLv2.0|Library for accessing puzzle files (.puz).|
|[Light Speed!](http://lightspeed.sourceforge.net/)|GNU LGPLv2.1|Light Speed! is an OpenGL-based program developed to illustrate the effects of special relativity on the appearance of moving objects.|
|[LMastermind](http://lgames.sourceforge.net/LMastermind/)|GNU GPLv2.0|Online mastermind game.|
|[LMemory](http://lgames.sourceforge.net/LMemory/)|GNU GPLv2.0|Online memory game.|
|[LPairs](http://lgames.sourceforge.net/index.php?project=LPairs)|GNU GPLv2.0|Classic memory card game.|
|[MasterMindy](https://f-droid.org/en/packages/eth.matteljay.mastermindy/)|MIT|Android version of mastermind game.|
|[MathWar](https://sourceforge.net/projects/mathwar-gtk/)|GNU GPLv2.0|Game for improving speed at solving math problems.|
|[Mnemosyne](http://www.mnemosyne-proj.org/)|GNU GPLv2.0|Mnemosyne software resembles a traditional flash-card program.|
|[Multiplication Station](http://www.pygame.org/project-Multiplication+Station-130-252.html)|GNU GPLv2.0|Multiplication Station will teach your child to add, subtract and multiply.|
|[Multiverse MMO Development Platform](http://sourceforge.net/projects/multiverse3d/)|MIT|Create your own sophisticated virtual world with little or no programming experience.|
|[Munchers](http://code.google.com/p/munchers/)|GNU GPLv2.0|A number munchers clone.|
|[The Number Race](http://www.thenumberrace.com/nr/home.php)|GNU GPLv2.0|Software designed for remediation of dyscalculia (or mathematical learning disabilities) in children aged 4-8 and for teaching number sense in kindergarten children.|
|[Nootka](http://nootka.sourceforge.net/)|GNU GPLv3.0|Nootka is a program to learn classical score notation. It helps to understand the rules of reading and writing scores and develops skills for playing and singing notes.|
|[Open Ear](https://f-droid.org/en/packages/com.openear.www/)|MIT|Ear training app for Android and IOS.|
|[OpenTeacher](http://openteacher.org/)|GNU GPLv3.0|OpenTeacher is an open source vocabulary training application that helps you learn a foreign language.|
|[Peg-e](https://github.com/gottcode/peg-e)|GNU GPLv3.0|Peg elimination game.|
|[Pendumito](http://krucenigmoj.tripod.com/pendumito.htm)|GNU GPLv2.0|Web based hangman game.|
|[Performous](http://performous.org/)|GNU GPLv2.0|A music game for singing (pitch detection), Karaoke, instrument playing and dancing.|
|[Photo Puzzle](https://github.com/klaytonkowalski/game-photo-puzzle)|MIT|Put photo pieces in order.|
|[President Matchup](http://sourceforge.net/projects/presidentmatchu/)|GNU GPLv2.0|Game to learn about US Presidents.|
|[PrimeShooter](http://thinkinghard.com/math/integers/PrimeShooter.html)|GNU GPLv2.0|Abstract browser based shooter game where the player shoots falling numbers with their divisors and scores points for clearing prime numbers.|
|[QeoDart](https://github.com/gulp21/QeoDart)|GNU GPLv3.0|Geography education game.|
|[Scalar](http://scalar.sourceforge.net/)|GNU GPLv2.0|Puzzle game.|
|[Scrabble3D](https://sourceforge.net/projects/scrabble/)|GNU GPLv3.0|Scrabble3D is a highly customizable Scrabble game that not only supports Classic Scrabble and Superscrabble but also 3D games and own boards. You can play local against the computer or connect to a game server to find other players.|
|[Scratch](http://scratch.mit.edu)|MIT|Scratch is a programming language that makes it easy to create your own interactive stories, animations, games, music and art and share your creations on the web.|
|[ScratchJr](https://github.com/LLK/scratchjr)|BSD 3 clause|Scratch redesigned for mobile devices.|
|[Simsu](https://github.com/gottcode/simsu)|GNU GPLv3.0|Sudoku game.|
|[Spotter](http://www.lightandmatter.com/spotter/spotter.html)|GNU GPLv2.0|Web based math and science quiz program.|
|[Starlanes](https://web.archive.org/web/20191230071944/http://www.barnsdle.demon.co.uk/game/starlanes.html)|GNU GPLv2.0 or later|Space trading game|
|[Sudoku](https://www.fltk.org/)|GNU LGPLv2.0|Sudoku game from FLTK test directory.|
|[Tanglet](https://gottcode.org/tanglet/)|GNU GPLv3.0|Boggle variant.|
|[Tangomon](https://tangomon-game.github.io/)|GNU GPLv3.0 or later|A monster battling game that can help with learning vocabulary.|
|[Terminalmath](Terminalmath)|GNU GPLv2.0|A textmode math practice game for school-level children that talks via eSpeak.|
|[Tetzle](https://github.com/gottcode/tetzle)|GNU GPLv3.0|Jigsaw puzzle that uses tetrominoes.|
|[toMOTko](http://sourceforge.net/projects/tomotko/)|GNU GPLv2.0|A flashcard application for learning foreign language vocabulary.|
|[TuxMath](https://github.com/tux4kids/tuxmath)|GNU GPLv2.0|TuxMath is an arcade game that helps kids practice their math facts.|
|[TuxMathScrabble](https://sourceforge.net/projects/tuxmathscrabble/)|GNU GPLv2.0|TuxMathScrabble is a math version of the classic word game Scrabble which challenges kids to construct compound equations and to consider multiple abstract possibilities.|
|[TuxTyping](https://github.com/tux4kids/tuxtype)|GNU GPLv2.0|Tux Typing is an educational typing tutor for children.|
|[TuxWordSmith](http://www.pygame.org/project-TuxWordSmith-335-2390.html)|GNU GPLv2.0|TuxWordSmith is an all in one application!  It's similar to the classic word game Scrabble, but with Unicode support for multiple languages.|
|[UltraStar Deluxe](http://ultrastardx.sourceforge.net/)|LGPLv2.1|Karaoke music game that can help improve pitch.|
|[Word Search Puzzle Generator](https://github.com/Magoninho/word-search-puzzle-generator)|MIT|Python program to generate a word search puzzle from a word file.|
|[Wordle](https://github.com/sehugg/libwordle/)|CC0v1.0|C library for wordle games and sample implementation.|
|[WordPlay](http://ironphoenix.org/tril/wordplay/)|GNU GPLv2.0|Games for improving anagram and scrabble skills.|
|[XWord](http://sourceforge.net/projects/wx-xword/)|GNU GPLv3.0|Interactive crossword solving program.|
|[xwords4](https://xwords.sourceforge.io/) and [more source](https://github.com/eehouse/xwords)|GNU GPLv2.0|Portable implementation of the rules of scrabble for mobile devices, Linux and Windows.|

## Maintainers

If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/lmemsm/delightful-educational-games/issues) with the tracker.

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/master/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC by 4.0](https://licensebuttons.net/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)
